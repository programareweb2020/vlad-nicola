import React from 'react';
import { BrowserRouter, Switch, Route, Link } from 'react-router-dom';
import Login from './Components/Login';
import Register from './Components/Register';
import Book from './Components/Book';
import Author from './Components/Author';
import BookList from './Components/BookList';
import AuthorList from './Components/AuthorList';

function App() {
    return (
        <BrowserRouter> 
                <Switch>
                    <Route exact path={"/"}>
                        <Link to="/login">Authentication</Link>
                        <br />
                        <Link to="/authorList">Authors</Link>
                        <br />
                        <Link to="/bookList">Books</Link>
                    </Route>
                    <Route path='/login' component={Login} />
                    <Route path='/register' component={Register} />
                    <Route path='/book/:id' exact component={Book} />
                    <Route path='/author/:id' exact component={Author} />
                    <Route path='/bookList' component={BookList} />
                    <Route path='/authorList' component={AuthorList} />
                </Switch>
        </BrowserRouter>
    );
}

export default App;