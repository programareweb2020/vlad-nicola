import React, { useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

function AuthorsList(props) {
    const [authors, setAuthors] = useState([]);
    const token = localStorage.getItem("token");
    const [requested, updateRequested] = useState(false);
    const [loading, updateLoading] = useState(true);

    if (!requested) {
        updateRequested(true);
        axios.get(`http://127.0.0.1:3000/api/v1/authors`, {
            headers: {
                  Authorization: `Bearer ${token}`
            }
        })
            .then(response =>  {
                setAuthors(response.data);
                updateLoading(false);
            })
            .catch(error => {
                console.log(error);
            })
    }

    return (
        <div>
            {loading? <h1>Authors list loading...</h1>:
            authors.map(x => <tr><Link to={`/authors/${x._id}`} >{x.firstName + " " + x.lastName + " | ID:" + x._id} </Link></tr>)}
        </div>
    );
}

export default AuthorsList;