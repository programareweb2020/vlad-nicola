import React, { useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

function BooksList(props) {
    const [books, setBooks] = useState([]);
    const token = localStorage.getItem("token");
    const [requested, updateRequested] = useState(false);
    const [loading, updateLoading] = useState(true);

    if (!requested) {
        updateRequested(true);
        axios.get(`http://127.0.0.1:3000/api/v1/books`, {
            headers: {
                  Authorization: `Bearer ${token}`
            }
        })
            .then(response =>  {
                setBooks(response.data);
                updateLoading(false);
            })
            .catch(error => {
                console.log(error);
            })
    }

    return (
        <div>
            {loading? <h1>Books list loading...</h1>:
            books.map(x => <tr><Link to={`/books/${x.id}`} params={{ id: x.id}}>{x.name + " " + x.author} </Link></tr>)}
        </div>
    );
}

export default BooksList;