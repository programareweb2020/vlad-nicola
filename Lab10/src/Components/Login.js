import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

function Login() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleUsername = event => {
        setUsername(event.target.value);
    };
    const handlePassword = event => {
        setPassword(event.target.value);
    };
    const handleLogin = event => {
        event.preventDefault();
        const data = {
            'username': username,
            'password': password
        };
        axios.post('http://127.0.0.1:3000/api/v1/users/login', data)
            .then((response) => {
                localStorage.setItem('token', response.data);
                alert('Login complete!')
            })
            .catch((error) => alert(error.response.data.error));
    };

    return (
        <form>
            <h3>Login</h3>
            <label>
                Username
                <input type='text' onChange={handleUsername} />
            </label>
            <br></br>
            <label>
                Password
                <input type='password' onChange={handlePassword} />
            </label>
            <br></br>
            <button onClick={handleLogin}>Login</button>
            <Link to='/register'>
                Don't have an account ? Click here to register.
            </Link>
        </form>
    );
}

export default Login;