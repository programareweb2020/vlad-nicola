import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

function Register() {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const handleUsername = event => {
        setUsername(event.target.value);
    };
    const handlePassword = event => {
        setPassword(event.target.value);
    };
    const handleRegister = () => {
        const data = {
            'username': username,
            'password': password
        };
        axios.post('http://127.0.0.1:3000/api/v1/users/register', data)
            .then((response) => {
                alert('Register complete!')
            })
            .catch((error) => alert(error.response.data.error));
    };

    return (
        <form>
            <h3>Register</h3>
            <label>
                Username
                <input type='text' onChange={handleUsername} />
            </label>
            <br></br>
            <label>
                Password
                <input type='password' onChange={handlePassword} />
            </label>
            <br></br>
            <button onClick={handleRegister}>Register</button>
            <Link to='/login'>
                Already have an account ? Click here to login.
            </Link>
        </form>
    );
}

export default Register;