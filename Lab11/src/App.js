import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ListGroup } from 'react-bootstrap';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Login from './Components/Login';
import Register from './Components/Register';
import Author from './Components/Author'
import AuthorList from './Components/AuthorList';
import BookList from './Components/BookList';
import Book from './Components/Book';
import NavBar from './Components/NavBar';
import './Components/Styling/App.scss'

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path={'/'} component={() =>
          <div>
            <NavBar />
            <div id='title'>
              {'Log in as a '}<b>{'user'}</b>{' to view lists, or as an '}<b>{'admin'}</b>{' to view and add new items.'}
            </div>
            <ListGroup as="ul">
              <ListGroup.Item as="li">
                <Link to="/authors">Authors list</Link>
              </ListGroup.Item>
              <ListGroup.Item as="li">
                <Link to="/books"> Books list</Link>
              </ListGroup.Item>
            </ListGroup>
          </div>
        } />
        <Route path={"/login"} component={Login} />
        <Route path={"/register"} component={Register} />
        <Route exact path={"/books"} component={BookList} />
        <Route path={"/books/:id"} component={Book} />
        <Route exact path={"/authors"} component={AuthorList} />
        <Route path={"/authors/:id"} component={Author} />
      </Switch>
    </Router >
  );
}

export default App;