import React from 'react';
import axios from 'axios';
import { useState, useEffect } from 'react';
import { ListGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom'
import NavBar from './NavBar';

function Author(props) {
    const [author, setAuthorData] = useState("");

    useEffect(() => {
        const { id } = props.match.params;
        const token = localStorage.getItem("token");

        axios
            .get(`http://localhost:3000/api/v1/authors/${id}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(response => {setAuthorData(response.data)})
            .catch(error => alert(error.response.data.error))
    }, [props])

    return (
        <div>
            <NavBar/>
            {Object.keys(author).map((keyName, i) => (
                <ListGroup.Item key={i}>
                    <div className="input-label">{keyName}: {author[keyName]}</div>
                </ListGroup.Item>
            ))}
            <ListGroup.Item as="li">
                <Link to="/authors">Go back</Link>
            </ListGroup.Item>
        </div>
    )
}

export default Author;