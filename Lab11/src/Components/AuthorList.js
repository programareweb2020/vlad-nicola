import React from 'react';
import axios from 'axios';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Table, Container, Row, Col, Form, Button } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'
import NavBar from './NavBar'

function AuthorList(props) {
    const [authors, setList] = useState([]);
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");

    function onChangeFirstName(e) {
        setFirstName(e.target.value);
    }

    function onChangelastName(e) {
        setLastName(e.target.value);
    }

    function onDeleteById(id) {
        const token = localStorage.getItem("token");

        axios.delete(`http://localhost:3000/api/v1/authors/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                getAuthors()
            })
            .catch(error => {
                alert(error.response.data.error)
            });
    }

    function onSubmitAuthor(e) {
        const author = {
            firstName: firstName,
            lastName: lastName
        };

        const token = localStorage.getItem("token");

        axios.post(`http://localhost:3000/api/v1/authors`, author, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                console.log(response.data);
            })
            .catch(error => {
                alert(error.response.data.error);
            });
    }

    function getAuthors() {
        const token = localStorage.getItem("token");
        axios.get(`http://localhost:3000/api/v1/authors`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                setList(response.data);
            })
            .catch(error => {
                alert(error.response.data.error);
            })
    }
    useEffect(() => {
        getAuthors();
    }, [])

    return (
        <Container>
            <NavBar />
            <Row>
                <Col>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>First Name</th>
                                <th className="d-none d-sm-table-cell">Last Name</th>
                                <th> Delete </th>
                            </tr>
                        </thead>
                        <tbody>
                            {authors.map(author =>
                                <tr>
                                    <td><Link to={`/authors/${author._id}`}>{author.firstName}</Link></td>
                                    <td className="d-none d-sm-table-cell">{author.lastName}</td>
                                    <td ><Button variant="danger" onClick={onDeleteById.bind(this, author._id)}> <FontAwesomeIcon icon={faTrash} /> </Button></td>
                                </tr>
                            )}

                        </tbody>
                    </Table>
                </Col>
            </Row>
            <Row className="justify-content-md-center" id="rowForm">
                <Col md="6" lg="4">
                    <br />
                    <h2 className="text-center">Add a new author here:</h2>
                    <Form>
                        <fieldset>
                            <legend class="scheduler-border"></legend>
                            <Form.Group>
                                <Form.Label>First name</Form.Label>
                                <Form.Control onChange={onChangeFirstName} type="text" placeholder="Enter first name" />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Last name</Form.Label>
                                <Form.Control onChange={onChangelastName} type="text" placeholder="Enter last name" />
                            </Form.Group>
                        </fieldset>
                        <div className="text-center">
                            <Button onClick={onSubmitAuthor} variant="info" type="submit" className="align-item-center">
                                Submit
                            </Button>
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>
    );
}

export default AuthorList;