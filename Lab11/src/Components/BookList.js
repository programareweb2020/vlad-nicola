import React from 'react';
import axios from 'axios';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Form, Table, Button, Container, Row, Col } from 'react-bootstrap'
import './Styling/BookList.scss';
import NavBar from './NavBar';

function BooksList(props) {

    const [books, setList] = useState([]);
    const [title, setTitle] = useState("");
    const [authorId, setAuthorId] = useState("");
    const [genre1, setGenre1] = useState("");
    const [genre2, setGenre2] = useState("");
    const [errorMessage, setErrorMessage] = useState("")

    function onChangeTitle(e) {
        setTitle(e.target.value);
    }

    function onChangeAuthorId(e) {
        setAuthorId(e.target.value);
    }

    function onChangeGenres1(e) {
        setGenre1(e.target.value);
    }

    function onChangeGenres2(e) {
        setGenre2(e.target.value);
    }

    function onSubmitBook(e) {
        let genres = [genre1];
        if (genre2 !== "")
            genres.push(genre2);

        const book = {
            name: title,
            authorId: authorId,
            genres: genres
        };

        const token = localStorage.getItem("token");

        console.log(book)
        axios.post(`http://localhost:3000/api/v1/books`, book, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => {
                console.log(response.data)
            })
            .catch(error => {
                alert(error.response.data.error)
            })
    }

    useEffect(() => {
        const token = localStorage.getItem("token");
        axios
            .get(`http://localhost:3000/api/v1/books`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(response => {
                setList(response.data)
            })
            .catch(error => alert(error.response.data.error))
    }, [])

    return (
        <Container>
            <NavBar />
            <Row>
                <Col>
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>Book name</th>
                                <th className="d-none d-sm-table-cell">Book author</th>
                                <th className="d-none d-sm-table-cell ">Book genres</th>
                            </tr>
                        </thead>
                        <tbody>
                            {books.map(book =>
                                <tr>
                                    <td><Link to={`/books/${book.id}`}>{book.name}</Link></td>
                                    <td className="d-none d-sm-table-cell">{book.author}</td>
                                    <td className="d-none d-sm-table-cell">{book.genres.join(', ')}</td>
                                </tr>
                            )}
                        </tbody>
                    </Table>
                </Col>
            </Row>
            <Row className="justify-content-md-center" id="rowForm">
                <Col md="6" lg="4">
                    <br />
                    <h2 className="text-center">Add a new book here:</h2>
                    <Form>
                        <fieldset>
                            <legend class="scheduler-border"></legend>
                            <Form.Group>
                                <Form.Label>Title</Form.Label>
                                <Form.Control onChange={onChangeTitle} type="text" placeholder="Enter title" />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Author ID</Form.Label>
                                <Form.Control onChange={onChangeAuthorId} type="text" placeholder="Enter author id" />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Genre 1</Form.Label>
                                <Form.Control onChange={onChangeGenres1} type="text" placeholder="Enter genre" />
                            </Form.Group>

                            <Form.Group>
                                <Form.Label>Genre 2</Form.Label>
                                <Form.Control onChange={onChangeGenres2} type="text" placeholder="Enter genre" />
                            </Form.Group>

                            <div className="text-center">
                                <Button onClick={onSubmitBook} variant="info" type="submit" className="align-item-center">
                                    Submit
                            </Button>
                            </div>
                        </fieldset>
                    </Form>
                </Col>
            </Row>

            { errorMessage && <h3 id="bottom-alert"> { errorMessage } </h3> }
        </Container >
    );
}

export default BooksList;