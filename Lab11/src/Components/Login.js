import React, { useState } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import axios from 'axios';
import './Styling/Login.scss';
import NavBar from './NavBar';

function Login(props) {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    function onChangeUserName(e) {
        setUsername(e.target.value)
    }

    function onChangeUserPassword(e) {
        setPassword(e.target.value)
    }

    function onSubmit(e) {
        const userObject = {
            username: username,
            password: password
        };

        console.log(userObject)
        axios.post(`http://localhost:3000/api/v1/users/login`, userObject)
            .then(response => {
                console.log(response.data)
                localStorage.setItem("token", response.data)
                alert('login ok')
            })
            .catch(error => alert(error.response.data.error));
    }


    return (
        <Container id="loginContainer">
            <NavBar />
            <Row className="justify-content-md-center">
                <Col md="6" lg="4">
                    <Form onSubmit={onSubmit}>
                        <Form.Group controlId="username">
                            <Form.Label>Username</Form.Label>
                            <Form.Control
                                autoFocus
                                type="username"
                                placeholder="Enter username"
                                onChange={onChangeUserName}
                            />
                        </Form.Group>
                        <Form.Group controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control
                                type="password"
                                placeholder="Password"
                                onChange={onChangeUserPassword}
                            />
                        </Form.Group>

                        <Link to="/">
                            <Button variant="info" type="submit" onClick={onSubmit}>
                                Login
                            </Button>
                        </Link>

                        <br/><br/>
                        <Link to="/register">
                            <div>
                                Don't have an account? Sign up here.
                            </div>
                        </Link>
                    </Form>
                </Col>
            </Row>
        </Container>
    );

}

export default Login;