import React from "react";
import "./Styling/NavBar.scss";
import { Nav, Navbar } from "react-bootstrap";

export default function NavBar() {
    return (
        <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
            <Navbar.Brand href="/">Home</Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="login">Login</Nav.Link>
                    <Nav.Link href="register">Register</Nav.Link>
                    <Nav.Link href="books">Books</Nav.Link>
                    <Nav.Link href="authors">Authors</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}