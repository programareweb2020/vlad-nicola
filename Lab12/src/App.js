import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Card from 'react-bootstrap/Card';
import Pagination from 'react-bootstrap/Pagination';
import { Button, Container, Row, Col } from 'react-bootstrap';

function User(props) {
    const [user, setUser] = useState([])

    useEffect(() => {
        axios.get(`http://jsonplaceholder.typicode.com/users/${props.userId}`)
            .then(response => setUser(response.data))
            .catch(error => console.log(error))
    }, [])
	return "by " + user.username + " aka " + user.name;
}

function App() {
  const [data, setData] = useState([]);
  const [active, setActive] = useState(1);
  const [items, setItems] = useState([]);


  useEffect(() => {
    axios.get("https://jsonplaceholder.typicode.com/albums")
    .then(response =>  {
      let d = response.data;
      let items = []
      for (let number = 1; number <= d.length / 10; number++) {
        items.push(
          <Pagination.Item key={number} active={number == active}>
            {number}
          </Pagination.Item>,
        );
	  }
	  setData(d);
      setItems(items);
    });
  }, [active]);

  function pageChanged(event)   {
    setActive(parseInt(event.target.text));
  }

	return (
		<div>
			<Pagination onClick={pageChanged}>{items}</Pagination>
				{data.map((value, index) => {
					if (parseInt(value.id / 10) + 1 === active)
						return (
							<div> 
								<Container>
									<Row>
									{[1].map (i => (
										<Col xs = '2'>
										<Card style={{ width: '18rem' }}>
										<Card.Img variant="top" src="holder.js/100px180" />
										<Card.Body>
										<Card.Title>{value.title}</Card.Title>
										<Card.Text>
										<User userId = {value.userId} />
										</Card.Text>
										<Button variant="primary">Go somewhere</Button>
										</Card.Body>
										</Card>
										</Col>
									))}
									</Row>
								</Container>
							</div>
						);
					else
						return <span key={index} />
				})}
		</div>
	);
}

export default App;
