const arr = [1, 2, 3, 4, 5, 6];

const func = (accumulator, currentValue) => { return accumulator + currentValue; }

console.log(arr.reduce(func));

module.exports = {func};
