const obj = require('./modul1.js');

function func(vec, parNum) {
	const par = parNum % 2 === 0;

	const filtered = vec.filter(function(x) {
		return (x % 2 === 0) === par;
	})

	return filtered.reduce(obj.func);
}

module.exports = {func};
