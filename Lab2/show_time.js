let moment = require('moment');
let time = moment().format('YYYY-LL-ZZThh:mm');

module.exports = { time };
