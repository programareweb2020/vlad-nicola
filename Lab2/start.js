const express = require('express');
const obj = require('./show_time.js');

const app = express();

app.get('/', (req, res) => {
    res.send(obj.time);
});

app.listen(3000);
