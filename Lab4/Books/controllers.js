const express = require('express');

const BooksService = require('./services.js');
const { validateFields } = require('../utils');
const { ServerError } = require('../errors');

const router = express.Router();

router.get('/', async (req, res, next) => {

    try {
        const books = await BooksService.getAll();
        res.json(books);

    } catch (err) {
        next(err);
    }
});

router.get('/:id', async (req, res, next) => {
    const { id } = req.params;

    try {
        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });

        const book = await BooksService.getById(parseInt(id));

        res.json(book);

    } catch (err) {
        next(err);
    }
});

router.post('/', async (req, res, next) => {
    const { name, author_id } = req.body;
    
    try {
        const fieldsToBeValidated = {
            first_name: {
                value: name,
                type: 'alpha'
            },
            id: {
                value: author_id,
                type: 'int'
            }
        };

        validateFields(fieldsToBeValidated);

        await BooksService.add(name, parseInt(author_id));

        res.status(201).end();

    } catch (err) {
        next(err);
    }
});

router.put('/:id', async (req, res, next) => {
    const { id } = req.params;
    const { name, author_id } = req.body;

    try {
        const fieldsToBeValidated = {
            id: {
                value: id,
                type: 'int'
            },
            first_name: {
                value: name,
                type: 'alpha'
            },
            author_id: {
                value: author_id,
                type: 'int'
            }
        };

        validateFields(fieldsToBeValidated);

        await BooksService.updateById(parseInt(id), name, parseInt(author_id));

        res.status(204).end();

    } catch (err) {
        next(err);
    }
});

router.delete('/:id', async (req, res, next) => {
    const { id } = req.params;

    try {
        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });

        
        await BooksService.deleteById(parseInt(id));

        res.status(204).end();
        
    } catch (err) {
        next(err);
    }
});

module.exports = router;
