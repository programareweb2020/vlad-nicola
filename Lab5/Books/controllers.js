const express = require('express');

const BooksService = require('./services.js');
const { validateFields } = require('../utils');
const { ServerError } = require('../errors');
const TokenService = require('../security/Jwt');
const { authorizeRoles } = require('../security/Roles');

const router = express.Router();

router.get('/', TokenService.authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res, next) => {
    try {
        const books = await BooksService.getAll();
        res.json(books);

    } catch (err) {
        next(err);
    }
});

router.get('/:id', TokenService.authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res, next) => {
    const { id } = req.params;

    try {
        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });

        const book = await BooksService.getById(parseInt(id));

        res.json(book);
    } catch (err) {
        next(err);
    }
});

router.post('/', TokenService.authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const { name, author_id } = req.body;
    
    try {
        const fieldsToBeValidated = {
            first_name: {
                value: name,
                type: 'alpha'
            },
            id: {
                value: author_id,
                type: 'int'
            }
        };

        validateFields(fieldsToBeValidated);
        await BooksService.add(name, parseInt(author_id));

		res.send('Success post book');
        res.status(201).end();
    } catch (err) {
        next(err);
    }
});

router.put('/:id', TokenService.authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
		id
	} = req.params;
    const {
		name,
		author_id
	} = req.body;

    try {
        const fieldsToBeValidated = {
            id: {
                value: id,
                type: 'int'
            },
            first_name: {
                value: name,
                type: 'alpha'
            },
            author_id: {
                value: author_id,
                type: 'int'
            }
        };

        validateFields(fieldsToBeValidated);
        await BooksService.updateById(parseInt(id), name, parseInt(author_id));

		res.send('Success put book');
        res.status(204).end();
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', TokenService.authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
		id
	} = req.params;

    try {
        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });
        await BooksService.deleteById(parseInt(id));
		
		res.send('Success delete book');
        res.status(204).end();
    } catch (err) {
        next(err);
    }
});

module.exports = router;
