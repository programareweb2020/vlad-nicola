const express = require('express');

const PublisherService = require('./services.js');
const { validateFields } = require('../utils');
const { ServerError } = require('../errors');
const TokenService = require('../security/Jwt');
const { authorizeRoles } = require('../security/Roles');

const router = express.Router();

router.get('/', TokenService.authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res, next) => {
    try {
        const publishers = await PublisherService.getAll();
        const names = publishers.map(item => item.name);
		
		console.log(publishers);
        res.json(names);
    } catch (err) {
        next(err);
    }
});

router.get('/:id', TokenService.authorizeAndExtractToken, authorizeRoles('admin', 'user'), async (req, res, next) => {
    const {
		id
	} = req.params;

    try {
        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });

        const publisher = await PublisherService.getById(parseInt(id));
        const publisherName = publisher.map(item => item.name);
    
        res.json(publisherName);
    } catch (err) {
        next(err);
    }
});

router.post('/', TokenService.authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
		name
	} = req.body;

    try {
        const fieldsToBeValidated = {
            name: {
                value: name,
                type: 'alpha'
            }
        };

        validateFields(fieldsToBeValidated);
        await PublisherService.add(name);

		res.send('Success post publishers');
        res.status(201).end();
    } catch (err) {
        next(err);
    }
});

router.put('/:id', TokenService.authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
		id
	} = req.params;
    const {
		name
	} = req.body;

    try {
        const fieldsToBeValidated = {
            id: {
                value: id,
                type: 'int'
            },
            name: {
                value: name,
                type: 'alpha'
            }
        };

        validateFields(fieldsToBeValidated);
        await PublisherService.updateById(parseInt(id), name);

		res.send('Success put publishers');
        res.status(204).end();
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', TokenService.authorizeAndExtractToken, authorizeRoles('admin'), async (req, res, next) => {
    const {
		id
	} = req.params;

    try {
        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });
        
        await PublisherService.deleteById(parseInt(id));

		res.send('Success delete publishers');
        res.status(204).end();
    } catch (err) {
        next(err);
    }
});

module.exports = router;
