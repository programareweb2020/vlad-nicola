const express = require('express');

const UsersService = require('./services.js');
const { validateFields } = require('../utils');

const router = express.Router();

router.post('/roles', async (req, res, next) => {
    const {
        value
    } = req.body;

    try {
        validateFields({
            value: {
                value,
                type: 'alpha'
            }
        });

        await UsersService.addRole(value);
		
		res.send('Success post users');
		res.status(201).end();
    } catch (err) {
        next(err);
    }
});

router.get('/roles', async (req, res, next) => {
	try {
		const roles = await UsersService.getRoles();

		res.json(roles);
	} catch(err) {
		next(err);
	}
});

router.post('/register', async (req, res, next) => {
    const {
        username,
        password,
        role_id
    } = req.body;

    try {
        const fieldsToBeValidated = {
            username: {
                value: username,
                type: 'alpha'
            },
            password: {
                value: password,
                type: 'ascii'
            },
            role_id: {
                value: role_id,
                type:'int'
            }
        };

        validateFields(fieldsToBeValidated);
        await UsersService.add(username, password, role_id);

		res.send('Success post register users');
        res.status(201).end();
    } catch (err) {
        next(err);
    }
});

router.get('/', async (req, res, next) => {
	try {
		const users = await UsersService.getUsers();

		res.json(users);
	} catch(err) {
		next(err);
	}
});

router.post('/login', async (req, res, next) => {
	const {
		username,
		password
	} = req.body;

	try {
		const fieldsToBeValidated = {
			username: {
				value: username,
				type: 'alpha'
			},
			password: {
				value: password,
				type: 'ascii'
			}
		};

		validateFields(fieldsToBeValidated);

		const token = await UsersService.authenticate(username, password);

		res.status(200).json(token);
	} catch (err) {
		next(err);
	}
});

router.delete('/:id', async (req, res, next) => {
    const {
        id
    } = req.params;

    try {

        validateFields({
            id: {
                value: id,
                type: 'int'
            }
        });

        await UsersService.deleteById(parseInt(id));

		res.send('Success delete users id');
        res.status(204).end();
    } catch (err) {
        next(err);
    }
});

module.exports = router;
