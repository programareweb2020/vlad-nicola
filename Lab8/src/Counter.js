import React from 'react';

class Counter extends React.Component {
	state = {
		counter: 0
	};

	onIncrementClicked = () => {
		this.setState({
			counter: this.state.counter + 1
		});
	};

	onDecrementClicked = () => {
		this.setState({
			counter: this.state.counter - 1
		});
	};

	onResetClicked = () => {
		this.setState({
			counter: 0
		});
	};

	render() {
		return (
			<>
				<h1>{this.state.counter}</h1>
				<button onClick={this.onIncrementClicked}>Increment</button>
				<button onClick={this.onDecrementClicked}>Decrement</button>
				<button onClick={this.onResetClicked}>Reset</button>
			</>
		);
	}
};

export default Counter;
