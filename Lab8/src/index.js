import React from 'react';
import ReactDOM from 'react-dom';

import Layout from './Layout';
import Header from './Header';
import Nav from './Nav';
import Counter from './Counter';
import Footer from './Footer';

ReactDOM.render(
  <React.StrictMode>
    <Layout>
		<Header />
		<Nav />
		<Counter />
		<Footer />
	</Layout>


  </React.StrictMode>,
  document.getElementById('root')
);
