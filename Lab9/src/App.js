import React from 'react';
import Layout from './Layout';
import Count from './Count';
import Nav from './Nav';
import Header from './Header';
import Footer from './Footer';

function App() {
  return (
	  <Layout>
		<Header />
		<Count />
		<Nav />
		<Footer />
	  </Layout>
  );
}

export default App;
