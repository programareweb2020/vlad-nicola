import React, { useState, useEffect } from 'react';
import './Styling.scss';

function Count(props) {
	const [state, setState] = useState(0);

	const increment = () => setState(state + 1);
	const decrement = () => setState(state - 1);
	const reset = () => setState(0);

	useEffect(() => {
		if (state === 0)
			alert('Contorul este 0');
	});

	return (
		<div className='Count'>
			<h1> {state} </h1>
			<button onClick={increment}>Increment</button>
			<button onClick={decrement}>Decrement</button>
			<button onClick={reset}>Reset</button>
		</div>
	);
}

export default Count;
