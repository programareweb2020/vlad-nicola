import React from 'react';
import './Styling.scss';

function Nav(props) {
	return (
		<div className='Nav'>
			<img alt='Logo' src='https://cdn.worldvectorlogo.com/logos/google-earth.svg'/>
            <a className='NavMenu' href='https://en.wikipedia.org/wiki/Europe'>Europe</a>
            <a className='NavMenu' href='https://en.wikipedia.org/wiki/Asia'>Asia</a>
            <a className='NavMenu' href='https://en.wikipedia.org/wiki/Africa'>Africa</a>
        
            <select className='NavDropdown'>
                <option>Logout</option>
            </select>
		</div>
	);
}

export default Nav;
